import React from 'react';
import { ProductCardItemProps } from "../../types/componentProps";
import { Card } from "react-bootstrap";
import { Link } from "react-router-dom";
import { Rating } from "../Common/Rating/Rating";
import "./ProductCardItem.scss"

/**
 * Functional component representing an individual product card item.
 * @param {ProductCardItemProps} props - The props containing the product data to display in the card item.
 * @returns JSX element representing the product card item.
 */
const ProductCardItem: React.FC<ProductCardItemProps> = ({ product }) => {
    return (
        <Card className="my-3 p-3 rounded card">
            <Link to={`/product/${product?.id}`}>
                <Card.Img src={product?.image} variant="top" style={{ width: '200px', height: '200px', textAlign: 'center' }} />
            </Link>
            <Card.Body>
                <Link to={`/product/${product?.id}`}>
                    <Card.Title as="div">
                        <strong>{product?.title}</strong>
                    </Card.Title>
                </Link>
                <Card.Text as="div">
                    <Rating
                        color={'#f8e825'}
                        value={product?.rating.rate}
                        text={`${product?.rating.count} reviews`}
                    />
                </Card.Text>
                <Card.Text as="h3">${product?.price}</Card.Text>
            </Card.Body>
        </Card>
    );
};

export default ProductCardItem;
