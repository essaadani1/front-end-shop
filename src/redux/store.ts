import { configureStore } from '@reduxjs/toolkit';
import productsReducer from './reducers/productsSlice';

// Configure the Redux store using the configureStore function
const store = configureStore({
    reducer: {
        // Assign the productsReducer to manage the 'products' slice of the store state
        products: productsReducer
    }
});

export default store;
