/**
 * Fetches all products data from the API.
 * @returns Promise containing the products data.
 */
export const fetchProductsData = async () => {
    const response = await fetch('https://fakestoreapi.com/products');
    return response.json();
};


/**
 * Fetches cart data by ID from the API.
 * @param id - The ID of the cart to fetch.
 * @returns Promise containing the cart data.
 */
export const fetchCart = async (id: string | undefined) => {
    if(id){
        const response = await fetch(`https://fakestoreapi.com/carts/${id}`);
        const data = await response.json();
        return data;
    }
    return []
}

/**
 * Fetches products data by their IDs from the API.
 * @param productIds - An array of product IDs to fetch.
 * @returns Promise containing the products data for the specified IDs.
 */
export const fetchProductsByIds = async (productIds: number []) => {
    const promises = productIds.map(async (productId) => {
        const response = await fetch(`https://fakestoreapi.com/products/${productId}`);
        return response.json();
    });
    return Promise.all(promises);
};
