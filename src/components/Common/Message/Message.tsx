import React from "react"
import { Alert } from "react-bootstrap"
import { MessageProps } from "../../../types/componentProps";

/**
 * Functional component for displaying messages.
 * @param variant - The variant of the message (e.g., 'success', 'danger', 'warning', 'info').
 * @param text - The text content of the message.
 * @returns JSX element displaying the message.
 */

const Message = ( {variant, text}: MessageProps ) => {
    return <Alert variant={variant}>{text}</Alert>
}

export default Message
