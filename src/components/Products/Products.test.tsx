import React from 'react';
import { render, waitFor } from '@testing-library/react';
import { Provider } from 'react-redux';
import { useQuery, UseQueryOptions, QueryKey, UseQueryResult } from 'react-query'; // Import necessary types
import Products from './Products'; // Ensure correct import path
import store from '../../redux/store';
import { Product } from "../../types/interfaces";
import { BrowserRouter } from "react-router-dom";

// Mock useQuery
jest.mock('react-query', () => ({
    useQuery: jest.fn(),
}));

// Mock products data
const mockProducts: Product[] = [
    {
        id: 1, title: 'Product 1', price: 332,
        description: 'description',
        category: 'Homme',
        image: '',
        rating: {rate:3, count:23}
    },
    {
        id: 2, title: 'Product 2', price: 332,
        description: 'description',
        category: 'Homme',
        image: '',
        rating: {rate:3, count:23}
    },
];

describe('Products Component', () => {
    it('should render loading state', async () => {
        // Mock useQuery to return loading state
        (useQuery as jest.Mock).mockReturnValue({isLoading: true});

        const {getByText} = render(
            <Provider store={store}>
                <Products/>
            </Provider>
        );

        await waitFor(() => {
            expect(getByText('Loading...')).toBeInTheDocument();
        });
    });

    it('should render products when data is available', async () => {
        // Mock useQuery to return products data
        (useQuery as jest.Mock).mockReturnValue({data: mockProducts});

        const {getByText} = render(
            <Provider store={store}>
                <BrowserRouter>
                    <Products />
                </BrowserRouter>
            </Provider>
        );

        await waitFor(() => {
            mockProducts.forEach(product => {
                expect(getByText(product.title)).toBeInTheDocument();
            });
        });
    });

    it('should render error message when there is an error', async () => {
        // Mock useQuery to return error state
        (useQuery as jest.Mock).mockReturnValue({isError: true});

        const {getByText} = render(
            <Provider store={store}>
                <Products/>
            </Provider>
        );

        await waitFor(() => {
            expect(getByText('Error fetching data')).toBeInTheDocument();
        });
    });
});
