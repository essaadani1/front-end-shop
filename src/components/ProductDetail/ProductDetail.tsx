import React from 'react';
import { Col, Row, Image, ListGroup, Button, Card } from 'react-bootstrap';
import { useParams, useNavigate } from 'react-router-dom';
import { Rating } from '../Common/Rating/Rating';
import { useQuery } from 'react-query';

/**
 * Functional component representing the product detail page.
 * Displays detailed information about a single product.
 * @returns JSX element representing the product detail page.
 */
const ProductDetail: React.FC = () => {
    // Get the product ID from the URL parameters
    const {id} = useParams<{ id: string }>();
    const navigate = useNavigate();

    // Fetch product data from the API using react-query
    const {data, isLoading, isError} = useQuery('products', async () => {
        const response = await fetch(`https://fakestoreapi.com/products/${id}`);
        const data = await response.json();
        console.log('data', data);
        return data;
    });

    // Display loading or error message while fetching data
    if (isLoading) return <div>Loading...</div>;
    if (isError) return <div>Error...</div>;

    // Function to handle adding the product to the cart
    const addToCartHandler = async ( productId: number ) => {
        await fetch('https://fakestoreapi.com/carts', {
            method: 'POST',
            body: JSON.stringify({
                date: '',
                products: [{productId: productId, quantity: 1}],
            }),
        });

        // Redirect to the cart page after adding the product
        navigate('/cart/5');
    };

    // Render the product detail page
    return (
        <Row>
            <Col md={6}>
                <Image src={data?.image} alt={data?.title} fluid/>
            </Col>
            <Col md={3}>
                <ListGroup variant="flush">
                    <ListGroup.Item>
                        <h3>{data?.title}</h3>
                    </ListGroup.Item>
                    <ListGroup.Item>
                        <Rating value={data?.rating?.rate} text={`${data?.rating?.count} reviews`}/>
                    </ListGroup.Item>
                    <ListGroup.Item>
                        <strong>Price :</strong> $ {data.price}
                    </ListGroup.Item>
                    <ListGroup.Item>
                        <strong>Description :</strong> {data.description}
                    </ListGroup.Item>
                </ListGroup>
            </Col>
            <Col md={3}>
                <Card>
                    <ListGroup variant="flush">
                        <ListGroup.Item>
                            <Row>
                                <Col>
                                    <strong>Price : </strong>
                                </Col>
                                <Col>
                                    <strong>$ {data?.price}</strong>
                                </Col>
                            </Row>
                        </ListGroup.Item>
                        <ListGroup.Item>
                            <Row>
                                <Col>
                                    <strong>Status : </strong>
                                </Col>
                                <Col>
                                    <strong>In Stock</strong>
                                </Col>
                            </Row>
                        </ListGroup.Item>

                        <ListGroup.Item>
                            <Button onClick={() => addToCartHandler(data?.id)} type="button" className="btn-block">
                                Add To Cart
                            </Button>
                        </ListGroup.Item>
                    </ListGroup>
                </Card>
            </Col>
        </Row>
    );
};

export default ProductDetail
