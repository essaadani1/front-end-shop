
describe('Adding items to the cart', () => {
    it('should add an item to the cart', () => {
        // Visit the product detail page
        cy.visit('http://localhost:3000/product/2');

        // Ensure the product detail page is loaded
        cy.contains('Loading...').should('not.exist');

        // Click the "Add To Cart" button
        cy.get('button').contains('Add To Cart').click();

        // Verify that the cart page is loaded after adding the item
        cy.url().should('include', 'http://localhost:3000/cart/5');

        // Verify that the added item is displayed in the cart
        cy.contains('Loading...').should('not.exist');
        cy.get('.cart-img').should('be.visible');
        cy.get('.cart-img').should('have.length.at.least', 1); // Adjust this selector if necessary
        cy.contains('Proceed To Checkout').should('be.visible');
    });
});

export {}
