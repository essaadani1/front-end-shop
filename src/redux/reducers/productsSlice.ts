import { createSlice } from "@reduxjs/toolkit";
import { PRODUCTS_REDUCER_NAME } from "../../utils/constants";
import { ProductsState } from "../../types/stateTypes";

// Initial state for the products slice
const initialState: ProductsState = {
    products: [],
    isLoading: false,
    isError: false
};

// Slice for managing products state
const productsSlice = createSlice({
    name: PRODUCTS_REDUCER_NAME,
    initialState,
    reducers: {
        // Reducer for setting products data and resetting loading and error states
        setProducts: ( state, action ) => {
            state.products = action.payload;
            state.isLoading = false;
            state.isError = false;
        },
        // Reducer for setting loading state while fetching data
        setLoading: ( state ) => {
            state.isLoading = true;
            state.isError = false;
        },
        // Reducer for setting error state if there's an issue fetching data
        setError: ( state ) => {
            state.isLoading = false;
            state.isError = true;
        }
    }
});

// Export action creators and reducer
export const {setProducts, setLoading, setError} = productsSlice.actions;
export default productsSlice.reducer;
