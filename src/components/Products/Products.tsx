import React, { useEffect } from 'react';
import { useQuery } from 'react-query';
import { Product } from '../../types/interfaces';
import { Col, Row } from 'react-bootstrap';
import ProductCardItem from '../ProductCardItem/ProductCardItem';
import { fetchProductsData } from '../../services/api';
import { useDispatch, useSelector } from 'react-redux';
import { setLoading, setProducts, setError } from '../../redux/reducers/productsSlice';
import { selectProducts } from '../../redux/selectors/productSelectors';

/**
 * Functional component representing the Products page.
 * Fetches and displays a list of products.
 * @returns JSX element representing the Products page.
 */
const Products: React.FC = () => {
    const dispatch = useDispatch();

    // Using React Query to fetch data
    const { data, isLoading, isError } = useQuery('products', fetchProductsData);

    // Get products from Redux store
    const products: Product[] = useSelector(selectProducts);

    // Update Redux store based on data fetching status
    useEffect(() => {
        if (data) {
            dispatch(setProducts(data));
        } else if (isLoading) {
            dispatch(setLoading());
        } else if (isError) {
            dispatch(setError());
        }
    }, [data, isLoading, isError, dispatch]);

    // Render loading or error message while fetching data
    if (isLoading) return <div>Loading...</div>;
    if (isError) return <div>Error fetching data</div>;

    // Render the list of products
    return (
        <>
            {products?.length > 0 && (
                <Row>
                    {products.map((product: Product) => (
                        <Col key={product.id} sm={12} md={6} lg={4} xl={3}>
                            <ProductCardItem product={product} />
                        </Col>
                    ))}
                </Row>
            )}
        </>
    );
};

export default Products;
