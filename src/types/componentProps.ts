import { Product } from "./interfaces";

export type ProductCardItemProps = {
    product: Product
};

export type RatingProps = {
    color?: string,
    value: number,
    text: string
}

export interface RouteParams {
    id: string;
}

export type MessageProps = {
    variant: string,
    text: string
}
