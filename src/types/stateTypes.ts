import { Product } from "./interfaces";
import { EStatus } from "./enums";

export type ProductsState = {
    products: Product[],
    isLoading: boolean,
    isError: boolean
}
