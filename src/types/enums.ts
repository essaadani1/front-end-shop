export enum EStatus {
    FAILED = 'failed',
    SUCCEEDED = 'succeeded',
    IDLE = 'idle'
}
