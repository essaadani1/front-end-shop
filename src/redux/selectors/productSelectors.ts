
/**
 * Selects the products array from the Redux store state.
 * @param state The Redux store state
 * @returns The array of products from the state
 */
export const selectProducts = ( state: any ) => state?.products.products;
