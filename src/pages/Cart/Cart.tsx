import React, { useEffect, useState } from "react"
import { Row, Col, ListGroup, Image, Form, Button, Card } from "react-bootstrap"
import { Link, useParams } from "react-router-dom"
import Message from "../../components/Common/Message/Message"
import { useQuery } from "react-query";
import { fetchCart, fetchProductsByIds } from "../../services/api";
import './Cart.scss';

/**
 * Functional component representing the Cart page.
 * Fetches and displays the items added to the cart.
 * Allows users to view, modify, and proceed to checkout.
 * @returns JSX element representing the Cart page.
 */
const Cart = () => {
    const { cartId } = useParams()
    const { data: cartData, isLoading: cartLoading, isError: cartError } = useQuery(['cart', cartId], () => fetchCart(cartId));
    const [total, setTotal] = useState(0)

    // Extract product IDs from cart data
    let productIds: any = []
    if (cartData && cartData.products && Array.isArray(cartData.products)) {
        productIds = cartData.products.map((item: any) => item.productId);
    }

    // Fetch product details based on product IDs
    const { data: productData, isLoading: productLoading, isError: productError } = useQuery(['products', productIds], () => fetchProductsByIds(productIds), {
        // This ensures that the query is enabled only when cartData is available and there are productIds to fetch
        enabled: !!cartData && productIds.length > 0, // This ensures that the query is enabled only when cartData is available and there are productIds to fetch
        onSuccess: (data) => {
            // Dispatch an action to store the product details in Redux store if needed
        }
    });

    // Calculate total price of all items in the cart
    useEffect(() => {

        if(productData)
            calculateTotal()

    }, [productData])


    // Render loading or error message while fetching data
    if (cartLoading || productLoading) return <div>Loading...</div>;
    if (cartError || productError) return <Message variant={'danger'} text={'Error fetching data'} />;
    if (!cartData || cartData.length === 0) return <Message variant={'info'} text={'No items in the cart'} />;


    //Handlers
   /* const removeFromCartHandler = (id) => {
        dispatch(removeFromCart(id))
    }*/


    // Calculate total price of all items in the cart

    const calculateTotal = () => {
        let sum = 0;

        if(productData){
            productData.forEach(product => {
                sum += product.price
            })
        }

        setTotal(sum)
    }

    // Render the list of items in the cart
    return  (
        <Row>
            <Col md={8}>
                    <ListGroup variant="flush">
                        {productData?.map((item: any) => (
                            <ListGroup.Item key={item.product}>
                                <Row>
                                    <Col md={2}>
                                        <Image
                                            src={item.image}
                                            alt={item.name}
                                            fluid
                                            rounded
                                            className={'cart-img'}
                                        ></Image>
                                    </Col>
                                    <Col md={3}>
                                        <Link to={`/product/${item.product}`}>{item.name}</Link>
                                    </Col>
                                    <Col md={2}>${item.price}</Col>
                                    <Col md={2}>
                                        <Form.Control
                                            as="input"
                                            value={item.quantity}
                                            onChange={(e) =>{}}
                                        >

                                        </Form.Control>
                                    </Col>
                                    <Col md={2}>
                                        <Button
                                            type="button"
                                            variant="light"
                                            onClick={() => {}}
                                        >
                                            <i className="fas fa-trash"></i>
                                        </Button>
                                    </Col>
                                </Row>
                            </ListGroup.Item>
                        ))}
                    </ListGroup>

            </Col>
            <Col md={4}>
                <Card>
                    <ListGroup>
                        <ListGroup.Item>
                            <>
                                <h2>
                                    Total
                                </h2>
                                ${total}
                            </>
                        </ListGroup.Item>
                        <ListGroup.Item>
                            <Button
                                type="button"
                                className="btn-block"
                                onClick={()=>{}}
                            >
                                Proceed To Checkout
                            </Button>
                        </ListGroup.Item>
                    </ListGroup>
                </Card>
            </Col>
        </Row>
    )

}

export default Cart
