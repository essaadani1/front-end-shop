import React from 'react';
import './App.css';
import { Container } from "react-bootstrap";
import { BrowserRouter as Router, Route,Routes  } from "react-router-dom";
import Home  from "./pages/Home/Home";
import ProductDetail from "./components/ProductDetail/ProductDetail";
import Cart from "./pages/Cart/Cart";
import Header from "./components/Layout/Header/Header";

function App() {
  return (
      <main className="py-3">
          <Router>
              <Header/>
              <main className="py-3">
                  <Container>
                      <Routes>
                          <Route path="/" element={<Home />} />
                          <Route path="/product/:id" element={<ProductDetail />} />
                          <Route path="/cart/:cartId" element={<Cart />} />
                      </Routes>
                  </Container>
              </main>

          </Router>
    </main>
  );
}

export default App;
